/**
 * Created by user on 01.06.17.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sell-it-not-found',
  templateUrl: 'not-found.component.html',
  styleUrls: ['not-found.component.scss']
})
export class NotFoundComponent {}

