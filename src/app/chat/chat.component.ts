/**
 * Created by andrei on 01.06.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sell-it-chat',
  templateUrl: 'chat.component.html',
  styleUrls: ['chat.component.scss']
})
export class ChatComponent {
  public homeDirectory = 'assets/img/';
}
