/**
 * Created by andrei on 02.06.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sell-it-chat-title',
  styleUrls: ['chat-title.component.scss'],
  templateUrl: 'chat-title.component.html'
})
export class ChatTitleComponent {}
