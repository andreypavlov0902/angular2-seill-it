/**
 * Created by andrei on 02.06.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sell-it-chat-input',
  templateUrl: 'chat-input.component.html',
  styleUrls: ['chat-input.component.scss']
})
export class ChatInputComponent {}
