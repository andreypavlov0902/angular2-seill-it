/**
 * Created by andrei on 02.06.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sell-it-chat-connect-item',
  templateUrl: 'chat-connect-item.component.html',
  styleUrls: ['chat-connect-item.component.scss']
})
export class ChatConnectItemComponent {
  public homeDirectory = 'assets/img/';
}
