/**
 * Created by andrei on 02.06.2017.
 */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// tslint:disable-next-line
import {
  ChatComponent,
  ChatTitleComponent,
  ChatMessageListComponent,
  ChatInputComponent,
  ChatConnectListComponent,
  ChatConnectItemComponent,
  ChatMessageItemComponent,
} from './';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: ChatComponent, pathMatch: 'full' }
    ])
  ],
  exports: [
    ChatComponent,
    ChatTitleComponent,
    ChatMessageListComponent,
    ChatInputComponent,
    ChatConnectListComponent,
    ChatConnectItemComponent,
    ChatMessageItemComponent,
  ],
  declarations: [
    ChatComponent,
    ChatTitleComponent,
    ChatMessageListComponent,
    ChatInputComponent,
    ChatConnectListComponent,
    ChatConnectItemComponent,
    ChatMessageItemComponent,
  ],
  providers: [],
})
export class ChatModule {}

