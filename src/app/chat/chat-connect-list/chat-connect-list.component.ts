/**
 * Created by andrei on 02.06.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sell-it-chat-connect-list',
  templateUrl: 'chat-connect-list.component.html',
  styleUrls: ['chat-connect-list.component.scss']
})
export class ChatConnectListComponent {}
