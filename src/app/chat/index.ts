/**
 * Created by andrei on 02.06.2017.
 */
export { ChatComponent } from './chat.component';
export { ChatTitleComponent } from './chat-title/chat-title.component';
export { ChatMessageListComponent } from './chat-message-list/chat-message-list.component';
export { ChatInputComponent } from './chat-input/chat-input.component';
export { ChatConnectListComponent } from './chat-connect-list/chat-connect-list.component';
export { ChatConnectItemComponent } from './chat-connect-item/chat-connect-item.component';
export { ChatMessageItemComponent } from './chat-message-item/chat-messate-item.component';

export { ChatModule } from './chat.module';

