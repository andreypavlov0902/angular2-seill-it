/**
 * Created by andrei on 02.06.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sell-it-chat-messages-list',
  templateUrl: './chat-message-list.component.html',
  styleUrls: ['chat-message-list.component.scss']
})
export class ChatMessageListComponent {}
