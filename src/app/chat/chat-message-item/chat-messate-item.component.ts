/**
 * Created by andrei on 02.06.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sell-it-chat-message-item',
  templateUrl: 'chat-messate-item.component.html',
  styleUrls: ['chat-messate-item.component.scss']
})
export class ChatMessageItemComponent {
  public homeDirectory = 'assets/img/';
}
