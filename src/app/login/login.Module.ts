/**
 * Created by andrei on 03.06.2017.
 */
import { NgModule } from '@angular/core';


import { SharedModule } from './../shared/shared.module';
import { LoginComponent } from './login.component';
import {CommonModule} from "@angular/common";
@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    LoginComponent
  ],
  declarations: [
    LoginComponent
  ],
  providers: []
})
export class LoginModule {}
