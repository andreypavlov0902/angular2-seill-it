/**
 * Created by andrei on 19.05.2017.
 */
import { Component, OnInit } from '@angular/core';
import { EventsService, DataProsuctsService } from './../shared/services/index';

@Component({
  selector: 'sell-it-index',
  templateUrl: './home.component.html',
  styleUrls: [
    './home.component.scss'
  ],
})
export class HomeComponent implements OnInit {
  public homeDirectory = 'assets/img/';
  constructor(
              private eventsService: EventsService,
              private dataProductsService: DataProsuctsService) {
  }
  public ngOnInit() {}
}
