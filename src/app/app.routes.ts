/*
import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';

import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
/!*  { path: '',      component: HomeComponent },
  { path: 'home',  component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'detail', loadChildren: './+detail#DetailModule'},
  { path: 'barrel', loadChildren: './+barrel#BarrelModule'},
  { path: '**',    component: NoContentComponent },*!/
];
*/

import { Routes } from '@angular/router';
import { DetailComponent } from './detail/detail.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './404/not-found.component';
import { ChatComponent } from './chat/chat.component';
import { AuthGuard } from './CanActivate';

export const ROUTES: Routes = [
  { path: 'details/:id', component: DetailComponent },
  {
    path: 'chat',
    // component: ChatComponent,
    loadChildren: 'app/chat/chat.module#ChatModule',
    canActivate: [AuthGuard]
    , pathMatch: 'full'
  },
  { path: 'login', component: LoginComponent },
  { path: '',      component: HomeComponent, pathMatch: 'full' },
  { path: '**',      component: NotFoundComponent },
];
