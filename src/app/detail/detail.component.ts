/**
 * Created by andrei on 21.05.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'detail',
  templateUrl: './detail.component.html',
  styleUrls: [
    'detail.component.scss'
  ]
})
export class DetailComponent {
  public homeDirectory = 'assets/img/';
}
