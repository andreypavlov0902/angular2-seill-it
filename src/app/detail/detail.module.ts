/**
 * Created by andrei on 03.06.2017.
 */
import { NgModule } from '@angular/core';
/*import { BrowserModule } from '@angular/platform-browser';*/
import { SharedModule } from './../shared/shared.module';
import { CommonModule } from '@angular/common';

import { DetailComponent } from './detail.component';
import {ChatComponent} from "../chat/chat.component";
import {RouterModule} from "@angular/router";
import {ChatModule} from "../chat/chat.module";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    /*ChatModule,*/
  ],
  exports: [
    DetailComponent
  ],
  declarations: [
    DetailComponent
  ],
  providers: []

})
export class DetailModule {}
