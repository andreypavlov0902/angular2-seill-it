/**
 * Created by andrei on 22.05.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sell-it-button-up',
  templateUrl: './button-up.component.html',
  styleUrls: [
    './button-up.component.scss'
  ]
})
export class ButtonUpComponent {}
