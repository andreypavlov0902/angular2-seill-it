/**
 * Created by andrei on 21.05.2017.
 */
export { HeaderComponent } from './header/header.component';
export { FooterComponent } from './footer/footer.component';
export { ButtonUpComponent } from './button-up/button-up.component';
export { SearchComponent } from './search/search.component';
export { UserBlockComponent } from './user-block/user-block.component';
