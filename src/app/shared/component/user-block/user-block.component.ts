/**
 * Created by andrei on 30.05.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sell-it-user-block',
  templateUrl: './user-block.component.html',
  styleUrls: ['./user-block.component.scss']
})
export class UserBlockComponent {
  public homeDirectory = 'assets/img/';
}
