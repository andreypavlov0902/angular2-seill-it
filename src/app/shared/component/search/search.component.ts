/**
 * Created by andrei on 30.05.2017.
 */
import { Component } from '@angular/core';

@Component({
  selector: 'sell-it-search',
  templateUrl: 'search.component.html',
  styleUrls: ['search.component.scss'],
})
export class SearchComponent {
  public str: string;
  public search($event) {
    console.log('$event', $event);
  }
}
