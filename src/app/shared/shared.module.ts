/**
 * Created by user on 22.05.17.
 */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {
  HeaderComponent,
  FooterComponent,
  ButtonUpComponent,
  SearchComponent,
  UserBlockComponent,
} from './component';
import { EventsService } from './services/events/events.service';
import { GetDataService } from './services/get-data.service';
import { UserBlockDirective, ScrollDirective } from './directive/';
import { RouterModule } from '@angular/router';
import {CommonModule} from "@angular/common";

@NgModule({
  exports: [
    HeaderComponent,
    FooterComponent,
    ScrollDirective,
    ButtonUpComponent,
    SearchComponent,
    UserBlockComponent,
    UserBlockDirective,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    ScrollDirective,
    ButtonUpComponent,
    SearchComponent,
    UserBlockComponent,
    UserBlockDirective,
  ],
  providers: [
    GetDataService,
    EventsService
  ]
})
export class SharedModule {
}
