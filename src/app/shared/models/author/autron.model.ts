/**
 * Created by andrei on 28.05.2017.
 */
export class Author {
  public id: number;
  public firstName: string;
  public lastName: string;
  public email: string;
  public photo: string;
}
