/**
 * Created by andrei on 31.05.2017.
 */
import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';

@Directive({ selector: '[sellItUserBlock]' })
export class UserBlockDirective {
 constructor(private el: ElementRef) {}
 @HostListener('click') public mouseClick() {
    let elem;
    elem = $(this.el.nativeElement).find('i[class*=fa]');
    elem.toggleClass('fa-bars');
    elem.toggleClass('fa-times');
    $('.user-data').toggleClass('show');
  }
}
