/**
 * Created by user on 22.05.17.
 */
import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: `[scroll]`
})
export class ScrollDirective {
  constructor(private el: ElementRef) {
  }
  @HostListener('window:scroll')
  public test() {
    let position: number = window.pageYOffset;
    if ( position > 300 ) {
      $(this.el.nativeElement).addClass('button-up__visibility');
    }else {
      $(this.el.nativeElement).removeClass('button-up__visibility');
    }
  }
  @HostListener('click')
  public upPage() {
    $('body,html').animate({scrollTop: 0}, 800);
    /*document.body.scrollTop = 0;*/
  }
}
