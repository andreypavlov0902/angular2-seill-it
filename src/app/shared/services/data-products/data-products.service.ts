/**
 * Created by andrei on 26.05.2017.
 */
import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Product } from './../../models/';

@Injectable()
export class DataProsuctsService {
  private urlExternals: string = 'http://fe-kurs.light-it.net:38000';
  private urlInternally: string = 'http://fe-kurs.light-it.loc:38000';
  private _limitQuery: number;
  private _offset: number = 0;
  private url: string;
  private next: string;
  private myProducts: Product[] = [];
  private _limitOffset: number = 0;
  constructor(private http: Http) {}

  public set limitQuery(value: number){
   if (typeof value === 'number' && value !== 0) {
     this._limitOffset = this._limitQuery = value;
   }
  }

  public setParamsRequest() {
    let limit: number = (this._limitQuery) ? this._limitQuery : 8;
    let params = new URLSearchParams();
    params.set ('limit', `${limit}`);
    params.set ('offset', `${this._offset}`);
    return params;
  }
  public setUrl(value: boolean): void {
    if (value) {
    this.url = this.urlExternals;
    return;
    }
    this.url = this.urlInternally;
  }
  private changeOffset(value: number) {
    this._offset += value;
  }
  public getProducts(): Observable<Product[]> {
    return this.http.get(this.url + `/api/poster/`, {search: this.setParamsRequest()})
      .map((response) => {
       return this.responsProcessed(response);
      });
  }
  public getProduct(id) {
    return this.http.get(this.url + `/api/poster/${id}/`)
            .map((response) => {
      return this.prouctProcessed(response);
            });
  }
  private prouctProcessed(response: Response) {
    let temp = response.json();
    let product = new Product(temp);
   // product.addAuthor( temp['author_details'] );
    return product;
  }
  private responsProcessed(response: Response) {
    let temp = response.json();
    this.changeOffset(this._limitOffset);

    temp.results.forEach((item) => {
       let product = new Product(item);
     //  product.addAuthor(item['author_details']);
       this.myProducts.push(product);
      });

    return this.myProducts;
  }
}
