/**
 * Created by andrei on 24.05.2017.
 */
import { Injectable } from '@angular/core';
import * as Rx from 'rxjs/Rx';

@Injectable()
export class EventsService {
  public listeners: any;
  public eventsSubject: any;
  private event: any;
  constructor() {

    this.listeners = {};
    this.eventsSubject = new Rx.Subject();
    this.event = Rx.Observable.from(this.eventsSubject);
    this.event.subscribe(({name, args}) => {
      if (this.listeners[name]) {
        for (let listener of this.listeners[name]) {
          listener(...args);
        }
      }
    });
  }
  public on(name, funcionListener) {
    if (!this.listeners[name]) {
      this.listeners[name] = [];
    }
    this.listeners[name].push(funcionListener);
  }
  public broadcast(name, ...args) {
    this.eventsSubject.next({
      name,
      args
    });
  }
  public clearAll() {
    this.listeners = {};
  }
  public clearOne(name) {
    if (this.listeners[name]) {
      delete this.listeners[name];
    }
  }
}
