/**
 * Created by andrei on 23.05.2017.
 */
import { Injectable } from '@angular/core';
import * as Rx from 'rxjs/Rx';
interface Data {
  img: string;
}
@Injectable()
export class GetDataService {
  private arrayData: Data[] = [];
  private arr: any[] = [
    {img: 'product0.jpg'},
    {img: 'product1.jpg'},
    {img: 'product2.jpg'},
    {img: 'product3.jpg'},
    {img: 'product0.jpg'},
    {img: 'product1.jpg'},
    {img: 'product2.jpg'},
    {img: 'product3.jpg'},
  ];
  public getData() {
    this.arrayData.push(...this.arr);
  }
  public getFromService() {
    return this.arrayData;
  }
}
