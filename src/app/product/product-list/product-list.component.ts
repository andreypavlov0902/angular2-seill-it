/**
 * Created by andrei on 31.05.2017.
 */
import { Component, OnInit } from '@angular/core';

import { GetProductService } from './../shared/get-product.service';
import { Product } from './../shared/product.model';
import { EventsService } from './../../shared/services/events/events.service';

@Component({
  selector: 'sell-it-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  public products: Product[];
  constructor(
    private getProductService: GetProductService,
    private eventsService: EventsService) {}
  public ngOnInit() {
    /* set params limitQuery = limitOffset */
    this.getProductService.limitQuery = 8;
    /*set url externals or internally*/
    this.getProductService.setUrl(false);
    this.getProductService.getProducts()
      .subscribe((data) => {
        this.products = data;
      });
    this.eventsService.on('addData', () => {
      this.getProductService.getProducts()
        .subscribe((data) => {
          this.products = data;
        });
    });
  }
}
