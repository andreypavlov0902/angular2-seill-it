/**
 * Created by andrei on 23.05.2017.
 */
import { Directive, HostListener, ElementRef, Output, EventEmitter } from '@angular/core';
import { EventsService } from './../../shared/services';
@Directive({
  selector: `[infinite-scroll]`
})
export class InfiniteScrollDirective {
  @Output()
  public addArr: EventEmitter<any> = new EventEmitter();
  constructor(private el: ElementRef,
              private eventService: EventsService) {
  }
@HostListener('window:scroll')
 public addToArrProducts() {
  let scrollHeight: number = this.el.nativeElement.scrollHeight;
  let position: number = window.pageYOffset;
  let pointRequest = scrollHeight - 800;
  if (position > pointRequest ) {
   this.eventService.broadcast('addData');
  }
}
}
