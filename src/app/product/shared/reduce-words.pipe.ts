/**
 * Created by andrei on 31.05.2017.
 */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'myReduceWords'})
export class ReduceWordsPipe implements PipeTransform {
  public transform(value: string): string {
    return (value.length > 23 ) ? value.substr(0, 20) + `...` : value;
  }
}
