/**
 * Created by andrei on 26.05.2017.
 */
import { Author } from './autron.model';
export class Product extends Author {
  public id: number;
  public title: string;
  public description: string;
  public price: number;
  public author: Author;
  public photo: string;
  constructor({
                id,
                title,
                description,
                price,
                photo_details,
                author_details
              }) {
    super();
    this.id = id;
    this.title = title;
    this.description = description;
    this.price = price;
    this.photo = photo_details;
    this.author = this.addAuthor(author_details);
  }
  public addAuthor({
                     id,
                     first_name: firstName,
                     last_name: lastName,
                     email,
                     photo: {
                       photo
                     }
                   }) {
    return {
      id,
      firstName,
      lastName,
      email,
      photo
    };
  }
  public getFullName(): string {
    return this.firstName + this.lastName;
  }

}
