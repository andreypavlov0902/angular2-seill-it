/**
 * Created by andrei on 31.05.2017.
 */
import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Product } from './product.model';

@Injectable()
export class GetProductService {
  private urlExternals: string = 'http://fe-kurs.light-it.net:38000';
  private urlInternally: string = 'http://fe-kurs.light-it.loc:38000';
  private limitOfQuery: number;
  private offset: number = 0;
  private url: string;
  private next: string;
  private myProducts: Product[] = [];
  private limitOffset: number = 0;
  constructor(private http: Http) {}

   set limitQuery(value: number) {
    if (typeof value === 'number' && value !== 0) {
      this.limitOffset = this.limitOfQuery = value;
    }
  }
  public setUrl(value: boolean): void {
    if (value) {
      this.url = this.urlExternals;
      return;
    }
    this.url = this.urlInternally;
  }
  public getProducts(): Observable<Product[]> {
    return this.http.get(this.url + `/api/poster/`, {search: this.setParamsRequest()})
      .map((response) => {
        return this.responsProcessed(response);
      });
  }
  public getProduct(id) {
    return this.http.get(this.url + `/api/poster/${id}/`)
      .map((response) => {
        return this.prouctProcessed(response);
      });
  }
  public setParamsRequest() {
    let limit: number = (this.limitOfQuery) ? this.limitOfQuery : 8;
    let params = new URLSearchParams();
    params.set ('limit', `${limit}`);
    params.set ('offset', `${this.offset}`);
    return params;
  }
  private changeOffset(value: number) {
    this.offset += value;
  }
  private prouctProcessed(response: Response) {
    let temp = response.json();
    return  new Product(temp);
  }
  private responsProcessed(response: Response) {
    let temp = response.json();
    this.changeOffset(this.limitOffset);

    temp.results.forEach((item) => {
      let product = new Product(item);
      this.myProducts.push(product);
    });

    return this.myProducts;
  }
}
