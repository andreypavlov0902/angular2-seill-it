/**
 * Created by andrei on 31.05.2017.
 */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { ProductListComponent } from './product-list/product-list.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { GetProductService } from './shared/get-product.service';
import { InfiniteScrollDirective } from './shared/infinite-scroll.directive';
import { ReduceWordsPipe } from './shared/reduce-words.pipe';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule
  ],
  exports: [
    ProductListComponent,
    ProductItemComponent,
    InfiniteScrollDirective,
    ReduceWordsPipe,
  ],
  declarations: [
    ProductListComponent,
    ProductItemComponent,
    InfiniteScrollDirective,
    ReduceWordsPipe,
  ],
  providers: [
    GetProductService
  ]
})
export class ProductModule {}
