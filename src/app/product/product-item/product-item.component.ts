/**
 * Created by andrei on 31.05.2017.
 */
import { Component, Input, OnChanges, OnInit } from '@angular/core';

import { Product } from './../shared/product.model';
@Component({
  selector: 'sell-it-product-item',
  templateUrl: 'product-item.component.html',
  styleUrls: ['product-item.component.scss']
})
export class ProductItemComponent implements OnInit, OnChanges{
  public homeDirectory = 'assets/img/';
  @Input()
  public products: Product[];

  public ngOnInit() {
  }
  public ngOnChanges() {
  }
}
