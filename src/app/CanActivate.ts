/**
 * Created by user on 05.06.17.
 */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import { Observable} from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {
  private access: boolean = true;
 constructor(/*private auth: AuthService*/ private router: Router) {}
 public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>| Promise<boolean>| boolean {
    if (this.access) {
      return true;
    } else {
      this.router.navigate(['details', 1]);
    }
  }
}
